GA Coding Dojo
-
Description, Tasks and Snippets related to the GA Coding Dojo's

---

### References

- https://ginkgo-analytics.atlassian.net/l/c/bQNEWKn7
- http://codingdojo.org/



### Kata's
The folder `katas` contains the kata's we've been practicing.