import logging

def add(input: str = None) -> str:
    '''adds numbers from a string'''
    logging.info(f"received string value {input}")

    return input