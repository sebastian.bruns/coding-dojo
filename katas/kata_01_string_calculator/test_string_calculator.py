from main import add

def test_tautology():
    assert True == True

def test_add_returns_same_string():
    '''output equals input'''
    assert add("22,1") == "22,1"